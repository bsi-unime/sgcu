package br.edu.unime.sgcu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SgcuApplication {

	public static void main(String[] args) {
		SpringApplication.run(SgcuApplication.class, args);
	}
}
