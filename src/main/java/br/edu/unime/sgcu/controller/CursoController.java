package br.edu.unime.sgcu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.unime.sgcu.model.entity.Curso;
import br.edu.unime.sgcu.model.service.CursoService;

@Controller
@RequestMapping("cursos")
public class CursoController {
	
	@Autowired
	private CursoService cursoService;
	
	public CursoController() {
	}
	
	@GetMapping
	public String listar(){
		return "curso/listar";
	}

	@PostMapping
	public String salvar(@Validated Curso curso, RedirectAttributes redirectAttributes){
		
		cursoService.save(curso);		
		return "redirect:/cursos";
	}

	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable Long id){
		
		this.cursoService.setReadonly("");
		return new ModelAndView("curso/editar").addObject("curso",cursoService.findById(id)); 
	}
	
	@GetMapping("/ver/{id}")
	public ModelAndView ver(@PathVariable Long id){
		
		ModelAndView mv = new ModelAndView("curso/editar").addObject("curso",cursoService.findById(id));
		
		this.cursoService.setReadonly("readonly");
		mv.addObject("readonly",this.cursoService.getReadonly());
		return mv; 
	}

	@RequestMapping(method = RequestMethod.GET, path = "/novo")
	public ModelAndView novo(){
		
		ModelAndView modelAndView = new ModelAndView("curso/editar").addObject("curso", new Curso());
		return modelAndView;
	}

	/* Model attributes */
	@ModelAttribute("cursos")
	public List<Curso> getCursos(){
		return cursoService.listAll();
	}
}