package br.edu.unime.sgcu.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.unime.sgcu.model.entity.Aluno;
import br.edu.unime.sgcu.model.entity.Curso;
import br.edu.unime.sgcu.model.service.AlunoService;
import br.edu.unime.sgcu.model.service.CursoService;
import br.edu.unime.sgcu.model.service.PessoaService;

@Controller
@RequestMapping("/alunos")
public class AlunoController {

	@Autowired	
	public AlunoService alunoService;
	@Autowired
	public PessoaService pessoaService;
	@Autowired
	public CursoService cursoService;

	public AlunoController() {	}

	@GetMapping
	public String listar(){
		return "aluno/listar";
	}

	@PostMapping
	public String salvar(@Validated Aluno aluno, RedirectAttributes redirectAttributes){
		
		alunoService.save(aluno);		
		return "redirect:/alunos";
	}

	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable Long id){
		
		this.pessoaService.setReadonly("");
		return new ModelAndView("aluno/editar").addObject("aluno",alunoService.findById(id)); 
	}
	
	@GetMapping("/ver/{id}")
	public ModelAndView ver(@PathVariable Long id){
		
		ModelAndView mv = new ModelAndView("aluno/editar").addObject("aluno",alunoService.findById(id));
		
		this.pessoaService.setReadonly("readonly");
		mv.addObject("readonly",this.pessoaService.getReadonly());
		return mv; 
	}

	@RequestMapping(method = RequestMethod.GET, path = "/novo")
	public ModelAndView novo(/*@Validated Aluno aluno, RedirectAttributes redirectAttributes*/){
		
		ModelAndView modelAndView = new ModelAndView("aluno/editar").addObject("aluno", new Aluno());
		return modelAndView;
	}

	/* Model attributes */
	@ModelAttribute("alunos")
	public List<Aluno> getAlunos(){
		return alunoService.listAll();
	}

	@ModelAttribute("estadosCivis")
	public List<String> getEstadosCivis(){
		return pessoaService.listaEstadoCivil();
	}
	
	@ModelAttribute("sexos")
	public List<Character> getSexos(){
		return Arrays.asList('M','F');
	}
	
	@ModelAttribute("cursos")
	public List<Curso> cursos(){
		return this.cursoService.listAll();
	}
}