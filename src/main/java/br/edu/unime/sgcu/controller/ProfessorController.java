package br.edu.unime.sgcu.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.unime.sgcu.model.entity.Aluno;
import br.edu.unime.sgcu.model.entity.Professor;
import br.edu.unime.sgcu.model.service.PessoaService;
import br.edu.unime.sgcu.model.service.ProfessorService;

@Controller
@RequestMapping("/professores")
public class ProfessorController {

	@Autowired
	public ProfessorService professorService;
	@Autowired
	public PessoaService pessoaService;

	public ProfessorController(){
	}

	@GetMapping
	public String listar(){
		return "professor/listar";
	}

	@PostMapping
	public String salvar(@Validated Professor professor, RedirectAttributes redirectAttributes){

		professorService.save(professor);		
		return "redirect:/professores";

	}

	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable Long id){

		this.pessoaService.setReadonly("readonly");
		return new ModelAndView("professor/editar").addObject("professor", professorService.findById(id));

	}
	
	@GetMapping("/ver/{id}")
	public ModelAndView ver(@PathVariable Long id){
		ModelAndView mv = new ModelAndView("professor/editar").addObject("professor",professorService.findById(id));
		this.pessoaService.setReadonly("readonly");
		mv.addObject("readonly",this.pessoaService.getReadonly());
		return mv; 
	}

	@RequestMapping(method = RequestMethod.GET, path = "/novo")
	public ModelAndView novo(){

		ModelAndView modelAndView = new ModelAndView("professor/editar").addObject("professor", new Aluno());
		return modelAndView;
	}

	/* Model attributes */
	@ModelAttribute("professores")
	public List<Professor> getProfessores(){
		return professorService.listAll();
	}

	@ModelAttribute("estadosCivis")
	public List<String> getEstadosCivis(){
		return pessoaService.listaEstadoCivil();
	}

	@ModelAttribute("sexos")
	public List<Character> getSexos(){
		return Arrays.asList('M','F');
	}
}