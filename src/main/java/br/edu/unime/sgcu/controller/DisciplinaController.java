package br.edu.unime.sgcu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.unime.sgcu.model.entity.Curso;
import br.edu.unime.sgcu.model.entity.Disciplina;
import br.edu.unime.sgcu.model.service.DisciplinaService;

@Controller
@RequestMapping("disciplinas")
public class DisciplinaController {
	
	@Autowired
	private DisciplinaService disciplinaService;
	
	public DisciplinaController() {
	}
	
	@GetMapping
	public String listar(){
		return "disciplina/listar";
	}

	@PostMapping
	public String salvar(@Validated Disciplina disciplina, RedirectAttributes redirectAttributes){
		
		disciplinaService.save(disciplina);		
		return "redirect:/disciplinas";
	}

	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable Long id){
		
		this.disciplinaService.setReadonly("");
		return new ModelAndView("disciplina/editar").addObject("disciplina",disciplinaService.findById(id)); 
	}
	
	@GetMapping("/ver/{id}")
	public ModelAndView ver(@PathVariable Long id){
		
		ModelAndView mv = new ModelAndView("disciplina/editar").addObject("disciplina",disciplinaService.findById(id));
		
		this.disciplinaService.setReadonly("readonly");
		mv.addObject("readonly",this.disciplinaService.getReadonly());
		return mv; 
	}

	@RequestMapping(method = RequestMethod.GET, path = "/novo")
	public ModelAndView novo(){
		
		ModelAndView modelAndView = new ModelAndView("disciplina/editar").addObject("disciplina", new Curso());
		return modelAndView;
	}

	/* Model attributes */
	@ModelAttribute("disciplinas")
	public List<Disciplina> getDisciplinas(){
		return disciplinaService.listAll();
	}
}