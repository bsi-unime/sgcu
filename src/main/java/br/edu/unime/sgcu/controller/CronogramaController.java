package br.edu.unime.sgcu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/cronogramas")
public class CronogramaController {
	
	public CronogramaController() {
	}
	
	@GetMapping
	public ModelAndView painel(){
		
		ModelAndView mv = new ModelAndView("/cronograma/painel");
		return mv;
	}
}