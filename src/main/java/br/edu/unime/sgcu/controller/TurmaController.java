package br.edu.unime.sgcu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.unime.sgcu.model.entity.Disciplina;
import br.edu.unime.sgcu.model.entity.Turma;
import br.edu.unime.sgcu.model.service.DisciplinaService;
import br.edu.unime.sgcu.model.service.TurmaService;

@Controller
@RequestMapping("turmas")
public class TurmaController {

	@Autowired
	private TurmaService turmaService;
	@Autowired
	private DisciplinaService disciplinaService;
	
	public List<String> listaDeDisciplinas; 

	public TurmaController() {
	}


	@GetMapping
	public String listar(){
		return "turma/listar";
	}

	@PostMapping
	public String salvar(@Validated Turma turma, RedirectAttributes redirectAttributes){

		turmaService.save(turma);		
		return "redirect:/turmas";
	}

	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable Long id){

		this.turmaService.setReadonly("");
		ModelAndView mv = new ModelAndView("turma/editar").addObject("turma",turmaService.findById(id));
		mv.addObject("listaDeDisciplinas",this.listaDeDisciplinas);
		return mv; 
	}

	@GetMapping("/ver/{id}")
	public ModelAndView ver(@PathVariable Long id){

		ModelAndView mv = new ModelAndView("turma/editar").addObject("turma",turmaService.findById(id));

		this.turmaService.setReadonly("readonly");
		mv.addObject("readonly",this.turmaService.getReadonly());
		mv.addObject("listaDeDisciplinas",this.listaDeDisciplinas);
		return mv; 
	}

	@RequestMapping(method = RequestMethod.GET, path = "/novo")
	public ModelAndView novo(){

		ModelAndView mv = new ModelAndView("turma/editar").addObject("turma", new Turma());
		mv.addObject("listaDeDisciplinas",this.listaDeDisciplinas);
		return mv;
	}

	@ModelAttribute("turmas")
	public List<Turma> getTurmas(){
		return turmaService.listAll();
	}

	@ModelAttribute("semestres")
	public List<String> listaSemestresLetivos(){
		return this.turmaService.semestresLetivos();
	}
	@ModelAttribute("turnos")
	public List<String> listaTurnos(){
		return this.turmaService.turnos();
	}
	@ModelAttribute("dias_semana")
	public List<String> listaDiasDaSemana(){
		return this.turmaService.diasDaSemana();
	}
	@ModelAttribute("disciplinas")
	public List<Disciplina> listaDisciplinas(){
		return this.disciplinaService.listAll();
	}
	
	public List<String> getListaDeDisciplinas() {
		return listaDeDisciplinas;
	}
	
	public void setListaDeDisciplinas(List<String> listaDeDisciplinas) {
		this.listaDeDisciplinas = listaDeDisciplinas;
	}
}