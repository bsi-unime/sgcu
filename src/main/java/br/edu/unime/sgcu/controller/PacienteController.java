package br.edu.unime.sgcu.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.unime.sgcu.model.entity.Paciente;
import br.edu.unime.sgcu.model.service.PacienteService;
import br.edu.unime.sgcu.model.service.PessoaService;

@Controller
@RequestMapping("/pacientes")
public class PacienteController {
	
	@Autowired
	private PacienteService pacienteService;
	@Autowired
	private PessoaService pessoaService;
	
	public PacienteController (){ }

	@GetMapping
	public String listar(){		
		return "paciente/listar";
	}
	
	@PostMapping
	public String salvar(@Validated Paciente paciente, RedirectAttributes redirectAttributes){
		pacienteService.save(paciente);
		return "redirect:/pacientes";
	}
	
	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable Long id){
		this.pessoaService.setReadonly("");						
		return new ModelAndView("paciente/editar").addObject("paciente", pacienteService.findById(id));
	}
	
	@GetMapping("/ver/{id}")
	public ModelAndView ver(@PathVariable Long id){
		ModelAndView mv = new ModelAndView("paciente/editar").addObject("paciente",pacienteService.findById(id));
		this.pessoaService.setReadonly("readonly");
		mv.addObject("readonly",this.pessoaService.getReadonly());
		return mv; 
	}
	
	@RequestMapping (method = RequestMethod.GET, path = "/novo")
	public ModelAndView novo(){
		ModelAndView modelAndView = new ModelAndView("paciente/editar").addObject("paciente", new Paciente());
		return modelAndView;
	}
	
		
	public void setPessoaService(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	
	public PessoaService getPessoaService() {
		return pessoaService;
	}

	public PacienteService getPacienteService() {
		return pacienteService;
	}

	public void setPacienteService(PacienteService pacienteService) {
		this.pacienteService = pacienteService;
	}
	
	/* Model attributes */
	@ModelAttribute("pacientes")
	public List<Paciente> getAlunos(){
		return pacienteService.listAll();
	}
	
	@ModelAttribute("estadosCivis")
	public List<String> getEstadosCivis(){
		return pessoaService.listaEstadoCivil();
	}
	
	@ModelAttribute("sexos")
	public List<Character> getSexos(){
		return Arrays.asList('M','F');
	}
}