package br.edu.unime.sgcu.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.unime.sgcu.model.entity.Disciplina;
import br.edu.unime.sgcu.model.repository.DisciplinaRepository;

@Service
public class DisciplinaService {
	
	private String readonly;
	
	@Autowired
	private DisciplinaRepository disciplinaRepository;
	
	public Disciplina save(Disciplina disciplina) {
		return disciplinaRepository.save(disciplina);
	}

	public Disciplina findById(Long id) {
		return disciplinaRepository.findById(id);
	}
	
	public List<Disciplina> listAll(){
		return this.disciplinaRepository.listAll();
	}
	
	public void setReadonly(String readonly) {
		this.readonly = readonly;
	}
	
	public String getReadonly() {
		return readonly;
	}
}