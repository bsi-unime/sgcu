package br.edu.unime.sgcu.model.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.edu.unime.sgcu.model.entity.Turma;

@Repository
public class TurmaRepository {
	
	public static List<Turma> turmas = new ArrayList<Turma>();
	
	public Turma findById(Long id){
		
		for (Turma turma : turmas) {
			
			if (turma.getId().equals(id)) {
				return turma;
			} 			
		}

		return null;

	}
	
	public List<Turma> listAll(){		

		return turmas;

	}
	
	public Turma save(Turma turma){
		
		if (turma.getId() != null) {
			
			turmas.set( (int) ( turma.getId()-1) , turma);
			return turma;
		}
		
		turma.setId((long)turmas.size()+1);
		
		turmas.add(turma);

		return turma;
	}			
}