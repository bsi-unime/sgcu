package br.edu.unime.sgcu.model.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.edu.unime.sgcu.model.entity.Aluno;

@Repository
public class AlunoRepository {
	
	public static List<Aluno> alunos = new ArrayList<Aluno>();
	
	public Aluno findById(Long id){
		
		for (Aluno aluno : alunos) {
			
			if (aluno.getId().equals(id)) {
				return aluno;
			} 			
		}

		return null;

	}

	public List<Aluno> listAll(){		

		return alunos;

	}

	public Aluno save(Aluno aluno){
		
		if (aluno.getId() != null) {
			
			alunos.set( (int) ( aluno.getId()-1) , aluno);
			return aluno;
		}
		
		aluno.setId((long)alunos.size()+1);
		
		alunos.add(aluno);

		return aluno;
	}
}
