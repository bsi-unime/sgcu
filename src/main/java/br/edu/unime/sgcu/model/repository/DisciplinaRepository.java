package br.edu.unime.sgcu.model.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.edu.unime.sgcu.model.entity.Disciplina;

@Repository
public class DisciplinaRepository {

	static List<Disciplina> disciplinas = new ArrayList<Disciplina>();

	public Disciplina findById(Long id){

		for (Disciplina disciplina : disciplinas) {

			if (disciplina.getId().equals(id)) {
				return disciplina;
			} 			
		}

		return null;

	}

	public List<Disciplina> listAll(){		

		return disciplinas;

	}

	public Disciplina save(Disciplina disciplina){

		if (disciplina.getId() != null) {

			disciplinas.set( (int) ( disciplina.getId()-1) , disciplina);
			return disciplina;
		}

		disciplina.setId((long)disciplinas.size()+1);

		disciplinas.add(disciplina);

		return disciplina;
	}

}
