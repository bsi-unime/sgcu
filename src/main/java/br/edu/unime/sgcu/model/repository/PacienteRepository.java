package br.edu.unime.sgcu.model.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.edu.unime.sgcu.model.entity.Paciente;

@Repository
public class PacienteRepository {
	
	private List<Paciente> pacientes = new ArrayList<Paciente>();
	
	public Paciente findById(Long id){		
		for (Paciente paciente : pacientes){
			if(paciente.getId().equals(id)){
				return paciente;
			}			
		}
		return null;				
	}
	
	public List<Paciente> listarTodos() {
		return pacientes;
	}
	
	public Paciente salvar(Paciente paciente){
		
		if (paciente.getId() != null){
			pacientes.set((int) (paciente.getId() - 1), paciente);
			return paciente;
		}
		
		paciente.setId((long)pacientes.size() + 1);		
		pacientes.add(paciente);
		
		return paciente;
	}
	

	public List<Paciente> getPacientes() {
		return pacientes;
	}

	public void setPacientes(List<Paciente> pacientes) {
		this.pacientes = pacientes;
	}		
	
}
