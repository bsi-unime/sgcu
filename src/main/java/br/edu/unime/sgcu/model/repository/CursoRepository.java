package br.edu.unime.sgcu.model.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.edu.unime.sgcu.model.entity.Curso;

@Repository
public class CursoRepository {

	static List<Curso> cursos = new ArrayList<Curso>();

	public Curso findById(Long id){

		for (Curso curso : cursos) {

			if (curso.getId().equals(id)) {
				return curso;
			} 			
		}

		return null;

	}

	public List<Curso> listAll(){		

		return cursos;

	}

	public Curso save(Curso curso){

		if (curso.getId() != null) {

			cursos.set( (int) ( curso.getId()-1) , curso);
			return curso;
		}

		curso.setId((long)cursos.size()+1);

		cursos.add(curso);

		return curso;
	}

}
