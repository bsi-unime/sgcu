package br.edu.unime.sgcu.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.unime.sgcu.model.entity.Professor;
import br.edu.unime.sgcu.model.repository.ProfessorRepository;

@Service
public class ProfessorService {
	
	@Autowired
	private ProfessorRepository professorRepository;

	public ProfessorService(){
		
	}
	
	public List<Professor> listAll(){
		return professorRepository.listAll();
	}
	
	public Professor save(Professor professor) {
		return professorRepository.save(professor);
	}

	public Professor findById(Long id) {
		
		return professorRepository.findById(id);
		
	}
	
}
