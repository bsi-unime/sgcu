package br.edu.unime.sgcu.model.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.unime.sgcu.model.entity.Turma;
import br.edu.unime.sgcu.model.repository.TurmaRepository;

@Service
public class TurmaService {

	private String readonly = "readonly";


	@Autowired
	private TurmaRepository turmaRepository;

	public TurmaService() {
	}

	public String getReadonly() {
		return readonly;
	}

	public void setReadonly(String readonly) {
		this.readonly = readonly;
	}

	public List<Turma> listAll(){
		return turmaRepository.listAll();
	}

	public Turma save(Turma turma) {
		return turmaRepository.save(turma);
	}

	public Turma findById(Long id) {

		return turmaRepository.findById(id);

	}

	public List<String> semestresLetivos(){
		return Arrays.asList(
				"2010.1",
				"2010.2",
				"2011.1",
				"2011.2",
				"2012.1",
				"2012.2",
				"2013.1",
				"2013.2",
				"2014.1",
				"2014.2",
				"2015.1",
				"2015.2",
				"2016.1",
				"2016.2",
				"2017.1",
				"2017.2",
				"2018.1",
				"2018.2",
				"2019.1",
				"2019.2",
				"2020.1",
				"2020.2",
				"2021.1",
				"2021.2",
				"2022.1",
				"2022.2",
				"2023.1",
				"2023.2"
				);
	}
	
	public List<String> turnos(){
		return Arrays.asList("Matutino","Vespertino","Noturno");
	}
	
	public List<String> diasDaSemana(){
		return Arrays.asList(	
							"Segunda-Feira",
							"Terça-Feira",
							"Quarta-Feira",
							"Quinta-Feira",
							"Sexta-Feira",
							"Sábado"
							);
	}

}