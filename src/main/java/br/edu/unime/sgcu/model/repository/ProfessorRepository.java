package br.edu.unime.sgcu.model.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.edu.unime.sgcu.model.entity.Professor;

@Repository
public class ProfessorRepository {

	public static List<Professor> professores = new ArrayList<Professor>();
			
	public Professor findById(Long id){
		
		for (Professor professor : professores) {
			
			if (professor.getId().equals(id)) {
				return professor;
			} 			
		}

		return null;

	}
	
	public List<Professor> listAll(){		

		return professores;

	}
	
	public Professor save(Professor professor){
		
		if (professor.getId() != null) {
			
			professores.set( (int) ( professor.getId()-1) , professor);
			return professor;
		}
		
		professor.setId((long)professores.size()+1);
		
		professores.add(professor);

		return professor;
	}			
	
}
