package br.edu.unime.sgcu.model.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;


@Service
public class PessoaService {
	
	private String readonly = "readonly";

	public List<String> listaEstadoCivil(){

		List<String> ec = new ArrayList<>();

		ec.add("");
		ec.add("Casado(a)");
		ec.add("Divorciado(a)");
		ec.add("Separado(a)");
		ec.add("Solteiro(a)");
		ec.add("Viúvo(a)");

		return ec;
	}

	public String getReadonly() {
		return readonly;
	}

	public void setReadonly(String readonly) {
		this.readonly = readonly;
	}

}
