package br.edu.unime.sgcu.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.unime.sgcu.model.entity.Paciente;
import br.edu.unime.sgcu.model.repository.PacienteRepository;

@Service
public class PacienteService {
	
	@Autowired
	private PacienteRepository pacienteRepository;
	
	public List<Paciente> listAll() {
		return pacienteRepository.listarTodos();
	}
	
	public Paciente save(Paciente paciente){
		return pacienteRepository.salvar(paciente);		
	}
	
	public Paciente findById(Long id){
		return pacienteRepository.findById(id);
	}

	public PacienteRepository getPacienteRepository() {
		return pacienteRepository;
	}

	public void setPacienteRepository(PacienteRepository pacienteRepository) {
		this.pacienteRepository = pacienteRepository;
	}

}
