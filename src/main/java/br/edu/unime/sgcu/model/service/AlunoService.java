package br.edu.unime.sgcu.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.unime.sgcu.model.entity.Aluno;
import br.edu.unime.sgcu.model.repository.AlunoRepository;

@Service
public class AlunoService {
	
	@Autowired
	private AlunoRepository alunoRepository;
	
	public AlunoService() {
	}

	public List<Aluno> listAll() {
		return alunoRepository.listAll();
	}

	public Aluno save(Aluno aluno) {
		return alunoRepository.save(aluno);
	}

	public Aluno findById(Long id) {
		
		return alunoRepository.findById(id);
		
	}
	
}
