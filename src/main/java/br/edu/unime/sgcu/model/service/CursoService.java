package br.edu.unime.sgcu.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.unime.sgcu.model.entity.Curso;
import br.edu.unime.sgcu.model.repository.CursoRepository;

@Service
public class CursoService {
	
	private String readonly;
	
	@Autowired
	private CursoRepository cursoRepository;
	
	public Curso save(Curso curso) {
		return cursoRepository.save(curso);
	}

	public Curso findById(Long id) {
		return cursoRepository.findById(id);
		
	}
	
	public List<Curso> listAll(){
		return this.cursoRepository.listAll();
	}
	
	public void setReadonly(String readonly) {
		this.readonly = readonly;
	}
	
	public String getReadonly() {
		return readonly;
	}
}