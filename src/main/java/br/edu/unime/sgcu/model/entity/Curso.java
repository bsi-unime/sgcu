package br.edu.unime.sgcu.model.entity;

import java.util.List;

public class Curso {

	private Long id = null;
	private String nome = null;
	private List<Aluno> alunos;
	private List<Disciplina> disciplinas_curso;
	
	public Curso() {
	}
	
	public Curso(String nome) {
		super();
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}
	
	public List<Disciplina> getDisciplinas_curso() {
		return disciplinas_curso;
	}
	
	public void setDisciplinas_curso(List<Disciplina> disciplinas_curso) {
		this.disciplinas_curso = disciplinas_curso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Curso other = (Curso) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}
}