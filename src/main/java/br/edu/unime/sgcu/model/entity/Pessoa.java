package br.edu.unime.sgcu.model.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

public abstract class Pessoa {

	private Long id;
	@NotNull(message="Preencha o nome")
	private String nome = null;
	private String rg = null;
	private String cpf = null;
	private String telefone = null;
	private String celular = null;
	private String telRecado = null;
	private String estadoCivil = null;
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataNascimento = null;
	private Character sexo = null;
	private String email = null;
	
	public Pessoa() {
		
	}
	
	
	public Pessoa(	String nome, String rg, String cpf, String telefone, String celular, String telRecado,
					String estadoCivil, Date dataNascimento, Character sexo, String email) {
		
		this.nome = nome;
		this.rg = rg;
		this.cpf = cpf;
		this.telefone = telefone;
		this.celular = celular;
		this.telRecado = telRecado;
		this.estadoCivil = estadoCivil;
		this.dataNascimento = dataNascimento;
		this.sexo = sexo;
		this.email = email;
	}

	/* Getters and Setters */
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getRg() {
		return rg;
	}
	
	public void setRg(String rg) {
		this.rg = rg;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getTelefone() {
		return telefone;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	public String getCelular() {
		return celular;
	}
	
	public void setCelular(String celular) {
		this.celular = celular;
	}
	
	public String getTelRecado() {
		return telRecado;
	}
	
	public void setTelRecado(String telRecado) {
		this.telRecado = telRecado;
	}
	
	public String getEstadoCivil() {
		return estadoCivil;
	}
	
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	
	public Date getDataNascimento() {
		return dataNascimento;
	}
	
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	public Character getSexo() {
		return sexo;
	}
	
	public void setSexo(Character sexo) {
		this.sexo = sexo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pessoa other = (Pessoa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}