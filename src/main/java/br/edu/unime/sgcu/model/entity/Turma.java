package br.edu.unime.sgcu.model.entity;

import java.util.List;

public class Turma {

	private Long id = null;
	private String semestreLetivo = null;
	private String turno;
	private String descricao;
	private List<String> diasSemana;
	private List<String> disciplinas_turma;
	private Professor professor;
	
	public Turma() {
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public List<String> getDiasSemana() {
		return diasSemana;
	}
	
	public void setDiasSemana(List<String> diasSemana) {
		this.diasSemana = diasSemana;
	}
	
	public String getSemestreLetivo() {
		return semestreLetivo;
	}
	
	public void setSemestreLetivo(String semestreLetivo) {
		this.semestreLetivo = semestreLetivo;
	}
	
	
	public List<String> getDisciplinas_turma() {
		return disciplinas_turma;
	}
	
	public void setDisciplinas_turma(List<String> disciplinas_turma) {
		this.disciplinas_turma = disciplinas_turma;
	}
	
	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	
	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Turma other = (Turma) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return this.semestreLetivo + " - " + this.turno + " - " + this.descricao ;
	}
	
}